import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'i-am-safe';
  isAuthenticated: boolean = false;

  constructor(private authService: AuthService, private router: Router) {
    const token = localStorage.getItem('token');
    if (token) {
      this.isAuthenticated = true;
      if (this.isAuthenticated) {
        this.router.navigate(['iamsafe']);
      }
    }
    this.authService.onAuthChange.subscribe(res => {
      this.isAuthenticated = res;
    });
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['.']);
  }
}
